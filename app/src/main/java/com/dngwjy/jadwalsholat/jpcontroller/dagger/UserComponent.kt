package com.dngwjy.jadwalsholat.jpcontroller.dagger

import com.dngwjy.jadwalsholat.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by wijaya on 29/06/19
 */
@Component(modules = [UserModule::class])
@Singleton
interface UserComponent {
    fun inject(mainActivity: MainActivity)
}