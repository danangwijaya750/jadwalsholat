package com.dngwjy.jadwalsholat.jpcontroller

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

/**
 * Created by wijaya on 29/06/19
 */
@Dao
interface UserDao {
    @Insert(onConflict = REPLACE)
    fun createUser(user: User)

    @Query("select * from user where name= :name")
    fun readUser(name:String):LiveData<User>

    @Query("delete from user where name= :name")
    fun deleteUser(name:String)
}