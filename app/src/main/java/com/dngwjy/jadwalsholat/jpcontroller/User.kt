package com.dngwjy.jadwalsholat.jpcontroller

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by wijaya on 29/06/19
 */
@Entity
data class User (
@PrimaryKey var name:String,
@ColumnInfo(name = "data") var data:String
)