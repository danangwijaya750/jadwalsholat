package com.dngwjy.jadwalsholat.jpcontroller

import androidx.room.Database
import androidx.room.RoomDatabase
import javax.inject.Singleton

/**
 * Created by wijaya on 29/06/19
 */
@Singleton
@Database(entities = arrayOf(User::class),version = 1,exportSchema = false)
abstract class UserDatabase:RoomDatabase(){
    abstract fun userDao():UserDao
}