package com.dngwjy.jadwalsholat.jpcontroller

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Singleton

/**
 * Created by wijaya on 29/06/19
 */
@Singleton
interface WebService {
    @GET("pray")
    fun getRoom(
        @Query("lat" )lat:String,
        @Query("long")longit:String
    ): Call<JadwalSholat>

    companion object{
        fun create(url:String):WebService{
            val retrofit=Retrofit.Builder().baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(WebService::class.java)
        }
    }
}