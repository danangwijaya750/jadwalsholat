package com.dngwjy.jadwalsholat.jpcontroller

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by wijaya on 29/06/19
 */
class UserRepository(private val webService: WebService,private val db:UserDao) {

    fun setUser(user:User){
        doAsync {
            db.createUser(user)
        }
    }

    fun getUser(s: String): LiveData<User>? {
        return db.readUser(s)
    }

    fun requestPraySchedule(locData:JSONObject,listener:MutableLiveData<JSONObject>){
    val lati=locData.getString("latitude")
    val longi=locData.getString("longitude")
    val aData=JSONObject()
        aData.put("isSuccess",false)
    webService.getRoom(lati,longi).enqueue(object :Callback<JadwalSholat>{

        override fun onFailure(call: Call<JadwalSholat>, t: Throwable) {
         aData.put("responseData",t.message)
            listener.value=aData
        }

        override fun onResponse(call: Call<JadwalSholat>, response: Response<JadwalSholat>) {
            Log.d(this@UserRepository::class.java.simpleName,response.toString())
            if(response.code()==200){
                val resp=response.body()!!.data
                val resp2=response.body()!!.time
                if(resp!=null&&resp2!=null){
                    aData.put("isSuccess",true)
                    val mData=JSONObject()
                    mData.put("subuh",resp.Fajr)
                    mData.put("luhur",resp.Dhuhr)

                    aData.put("responseData",mData.toString())
                    setUser(User("schedule",mData.toString()))

                }else{
                    aData.put("responseData","data error")
                }
            }else{
                aData.put("responseData",response.code().toString())
            }
            listener.value=aData
        }

    })
}
}