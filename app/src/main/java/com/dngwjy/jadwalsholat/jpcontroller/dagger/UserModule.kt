package com.dngwjy.jadwalsholat.jpcontroller.dagger

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dngwjy.jadwalsholat.jpcontroller.UserDatabase
import com.dngwjy.jadwalsholat.jpcontroller.UserRepository
import com.dngwjy.jadwalsholat.jpcontroller.WebService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by wijaya on 29/06/19
 */
@Module
@Singleton
class UserModule(private val context: Context,private val mUrl:String) {
    @Provides
    @Singleton
    fun getRepo():UserRepository{
        return UserRepository(
            WebService.create(mUrl),
            Room.databaseBuilder(
                context,UserDatabase::class.java,"report_maker"
            ).build().userDao()
        )
    }
}