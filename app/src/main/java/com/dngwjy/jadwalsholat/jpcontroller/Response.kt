package com.dngwjy.jadwalsholat.jpcontroller

import java.util.*

/**
 * Created by wijaya on 29/06/19
 */
class JadwalSholat(
    var data:Data,
    var time:Time
)
class Data(
    var Fajr:String,
    var Dhuhr:String,
    var Asr:String,
    var Maghrib:String,
    var Isha:String
)
class Time(
    var date: Date
)