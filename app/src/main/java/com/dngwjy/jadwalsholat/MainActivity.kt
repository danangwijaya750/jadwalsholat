package com.dngwjy.jadwalsholat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.dngwjy.jadwalsholat.fragment.HomeFragment
import com.dngwjy.jadwalsholat.jpcontroller.UserRepository
import com.dngwjy.jadwalsholat.jpcontroller.dagger.DaggerUserComponent
import com.dngwjy.jadwalsholat.jpcontroller.dagger.UserModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var userRepo:UserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerUserComponent.builder().userModule(UserModule(this,"https://time.siswadi.com/"))
            .build().inject(this)
        changeFragment(HomeFragment())
    }

    fun changeFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame1,fragment)
            .commitAllowingStateLoss()
    }
}
