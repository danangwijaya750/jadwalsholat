package com.dngwjy.jadwalsholat.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.dngwjy.jadwalsholat.jpcontroller.User
import com.dngwjy.jadwalsholat.jpcontroller.UserRepository
import org.json.JSONObject


class HomeViewModel : ViewModel() {
    // TODO: Implement the ViewModel
    private var userRepository:UserRepository?=null
    private var scheduleDb:LiveData<User>?=null
    private var scheduleListener=MutableLiveData<JSONObject>()

    fun init(userRepository: UserRepository){
        this.userRepository=userRepository
        if(scheduleDb==null){
            scheduleDb=userRepository.getUser("schedule")
        }
    }
    fun getScheduleDatabase():LiveData<User>?{
        return scheduleDb
    }
    fun getScheduleListener():LiveData<JSONObject>?{
        return scheduleListener
    }

    @SuppressLint("MissingPermission")
    fun getLocate(locationManager: LocationManager){
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,10f,
            object:LocationListener{
                override fun onLocationChanged(location: Location?) {
                    val latitude= location?.latitude
                    val longitude=location?.longitude
                    val oData= JSONObject()
                    oData.put("longitude",longitude)
                    oData.put("latitude",latitude)
                    userRepository?.requestPraySchedule(oData,scheduleListener)
                }

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                }

                override fun onProviderEnabled(provider: String?) {

                }

                override fun onProviderDisabled(provider: String?) {

                }

            })
    }

    fun enableGps(context:Context){

    }
}
